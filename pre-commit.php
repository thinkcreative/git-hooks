#!/usr/bin/php
<?php
/**
 * @author    Serhey Dolgushev <dolgushev.serhey@gmail.com>
 * @date      31 Dec 2015
 * @copyright ContextualCode
 */
$validationRules = array(
    array(
        'type'               => 'PHP',
        'executable_binary'  => 'php',
        'installation_guide' => null,
        'file_pattern'       => '/\.ph(tml|p)$/',
        'command'            => 'php -l %file% 2>/dev/null'
    ),
    array(
        'type'               => 'JavaScript',
        'executable_binary'  => 'jshint',
        'installation_guide' => "1. Go to and install NodeJS\n2. sudo npm install -g jshint",
        'file_pattern'       => '/\.js$/',
        'command'            => 'jshint %file% 2>/dev/null'
    ),
    array(
        'type'               => 'CSS',
        'executable_binary'  => 'csslint',
        'installation_guide' => "1. Go to and install NodeJS\n2. sudo npm install -g csslint",
        'file_pattern'       => '/\.css$/',
        'command'            => 'csslint %file% 2>/dev/null'
    ),
    array(
        'type'               => 'DebugInfo',
        'executable_binary'  => 'cat',
        'installation_guide' => null,
        'file_pattern'       => '/\.ph(tml|p)|js$/',
        'command'            => 'cat %file% | grep "var_dump\|console.log"',
        'callback'           => function( array $output, $return ) {
            return count( $output ) === 0;
        }
    ),
    array(
        'type'               => 'Twig',
        'executable_binary'  => 'twig-lint.phar',
        'installation_guide' => "1. cd /usr/bin\n2. sudo wget https://asm89.github.io/d/twig-lint.phar\n3. sudo chmod 0755 twig-lint.phar",
        'file_pattern'       => '/\.twig$/',
        'command'            => 'php twig-lint.phar lint %file%'
    )
);

$status       = 0;
$changedFiles = array();
$invalidFiles = array();
$return       = 0;
exec( 'git rev-parse --verify HEAD 2> /dev/null', $changedFiles, $return );
$against      = $return == 0 ? 'HEAD' : '4b825dc642cb6eb9a060e54bf8d69288fbee4904';
exec( "git diff-index --cached --name-only {$against}", $changedFiles );

foreach( $validationRules as $validationRule ) {
    echo str_repeat( '-', 80 ) . "\n";
    echo "Validating " . $validationRule[ 'type' ] . " files ...\n\n";

    // Check if current validation rule is enabled
    $tmp       = null;
    exec( 'git config --global --get validations.' . $validationRule[ 'type' ], $tmp );
    $tmp       = count( $tmp ) ? $tmp[ 0 ] : null;
    $isEnabled = $tmp === null || (int) $tmp !== 0;
    if( $isEnabled === false ) {
        echo "This validation rule is disabled, so skipping it.\n";
        echo "It can be enabled by running the following command:.\n\n";
        echo "\tgit config --global validations." . $validationRule[ 'type' ] . " 1\n\n";
        continue;
    }

    // Check if required binary for current validation rule is installed
    exec( 'type ' . $validationRule[ 'executable_binary' ] . ' > /dev/null 2>&1', $output, $return );
    if( $return !== 0 ) {
        echo "\033[0;31mThis validation rule depends on third party executables which are not installed.\n";
        echo "In order to install them, please follow installation guide:\033[0m\n\n";
        echo $validationRule[ 'installation_guide' ] . "\n\n";

        continue;
    }

    // Go through all changed files and check each of them against current validation rule
    foreach( $changedFiles as $file ) {
        if( !preg_match( $validationRule[ 'file_pattern' ], $file ) ) {
            continue;
        }

        // We are skipping removed/renamed files
        if( !file_exists( $file ) ) {
            continue;
        }

        // Running current validation rule command
        $output  = array();
        $command = str_replace( '%file%', $file, $validationRule[ 'command' ] );
        exec( $command, $output, $return );

        // If current validation rules has callback, call it to determine if current file is valid
        // Otherwise we are using command's exit status code
        if( isset( $validationRule[ 'callback' ] ) && is_callable( $validationRule[ 'callback' ] ) ) {
            $callback = $validationRule[ 'callback' ];
            if( $callback( $output, $return ) === true ) {
                continue;
            }
        } else {
            if( $return == 0 ) {
                continue;
            }
        }

        echo "\033[1m" . $file . ":\033[0m\n";
        $invalidFiles[] = $file;
        echo "\033[0;31m";
        foreach( $output as $key => $line ) {
            if( empty( $line ) ) {
                continue;
            }

            echo $line, "\n";
        }
        echo "\033[0m\n";

        $status = 1;
    }
}

if( $status === 1 ) {
    echo "\033[1m" . str_repeat( '-', 80 ) . "\n";
    echo "Commit aborted! Please fix following files:\n";
    foreach( $invalidFiles as $file ) {
        echo '- ' . $file . "\n";
    }
    echo "\nAdd and commit them after they will be fixed.\n";
    echo "\nAlso you can use \"--no-verify\" flag to make commit without validation.\n";
    echo "Please note, it should be used only in very very very (!) rare cases\033[0m\n";
}
echo str_repeat( '-', 80 ) . "\n\n";

exit( $status );
